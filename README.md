# README

Cloud services (AWS, DigitalOcean, etc.) subdomains reverse proxy using Docker and NGINX. Run multiple projects from one instance, using different subdomains.

SSL connection configured with [Let's Encrypt webproxy](https://github.com/evertramos/docker-compose-letsencrypt-nginx-proxy-companion).

## App Information

App Name: subdomains

Created: July 2019

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/subdomains)

## To run

1. Create and set up cloud server instance
2. Install Docker and docker-compose
3. Copy _Let's Encrypt webproxy_ project folder to server and change IP
4. Copy _subdomains_ project folder to server
5. Update _docker-compose_ file with projects
6. Update _/reverse-proxy/default.conf_ routes
7. Start _Let's Encrypt webproxy_
8. Start _subdomains_

Last updated: 2025-01-25
